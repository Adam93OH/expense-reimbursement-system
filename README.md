# Expense Reimbursement System


# Project Description
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

# Technologies Used

- Java
- MariaDB
- JDBC
- Javalin
- HTML
- CSS
- Javascript
- JUnit
- Selenium
- Mockito



# Features
- List of features ready and TODOs for future development
- Employees can log in and submit reimbursement requests for various types of expenses
- Managers can log in and view pending reimbursements and approve or deny them



# Getting Started
- git clone https://gitlab.com/Adam93OH/expense-reimbursement-system.git
- Deploy the application using Javalin by running the MainDriver class within src/main/java/com/projectone



# Usage

- Navigate to http://localhost:9009/html/index.html in your browser
- To log in as a user, use username: "aalouani", password: "password"
- To log in as a manager, use username: "bossman", password: "boss123"

