package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;
import com.example.service.ReimbursementService;

public class ReimbServiceTest {

	@Mock
	private ReimbursementDao mockedDao;
	private ReimbursementService testService =  new ReimbursementService(mockedDao);
	private Reimbursement testReimbursement;
	private Reimbursement testReimbursement2;
	private List<Reimbursement> rList;
	
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Timestamp t = null;
		Blob b = null;
		testService = new ReimbursementService(mockedDao);
		testReimbursement = new Reimbursement(1, 400, t, t, "description", b, 1, 2, 1, 4);
		testReimbursement2 = new Reimbursement(2, 400, t, t, "description", b, 1, 2, 1, 4);
		rList = new ArrayList<>();
		rList.add(testReimbursement);
		when(mockedDao.findById(1)).thenReturn(testReimbursement);
		when(mockedDao.findById(2)).thenReturn(testReimbursement2);
		when(mockedDao.selectAllbyAuthor(1)).thenReturn(rList);
		when(mockedDao.selectAllPending()).thenReturn(rList);
		when(mockedDao.selectAll()).thenReturn(rList);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testGetReimbursementByIdSuccess() {
		assertEquals(testService.getReimbursementsById(1), rList);
	}
	
	@Test
	public void testGetReimbursementPendingSuccess() {
		assertEquals(testService.getAllPendingReimbursements(), rList);
	}
	
	@Test
	public void testGetAllReimbursementSuccess() {
		assertEquals(testService.getAllReimbursements(), rList);
	}
}
