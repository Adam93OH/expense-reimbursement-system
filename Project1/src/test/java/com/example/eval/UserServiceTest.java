package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.UserDao;
import com.example.model.User;
import com.example.service.UserService;

public class UserServiceTest {
	
	@Mock
	private UserDao mockedDao;
	private UserService testService =  new UserService(mockedDao);
	private User testUser;
	private User testUser2;
	
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new UserService(mockedDao);
		testUser = new User(1,"aalouani","password", "Adam", "Alouani", "aalouani@gmail.com", 1);
		testUser2 = new User(2,"aalouani2","password", "Adam", "Alouani", "aalouani@gmail.com", 2);
		when(mockedDao.findById(1)).thenReturn(testUser);
		when(mockedDao.findByName("aalouani")).thenReturn(testUser);
		when(mockedDao.findByName("aalouani2")).thenReturn(testUser2);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testGetUserByIdSuccess() {
		assertEquals(testService.getUserById(1), testUser);
	}
	
	@Test
	public void testGetUserByNameSuccess() {
		assertEquals(testService.getUserByName("aalouani"), testUser);
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetUserFailure() {
		assertEquals(testService.getUserByName("okay"), null);
	}
	
	@Test
	public void testUserVerifySuccess() {
		assertTrue(testService.userVerify("aalouani", "password"));
	}
	
	@Test
	public void testUserVerifyFailure() {
		assertFalse(testService.userVerify("aalouani", "1234"));
	}
	
	@Test
	public void testManagerVerifySuccess() {
		assertTrue(testService.userVerifyManager("aalouani2"));
	}
	
	@Test
	public void testManagerVerifyFailure() {
		assertFalse(testService.userVerifyManager("aalouani"));
	}

}
