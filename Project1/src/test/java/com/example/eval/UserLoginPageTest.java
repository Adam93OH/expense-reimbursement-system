package com.example.eval;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserLoginPageTest {
	
	private UserLoginPage page;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		String filePath = "src/test/Resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception{
		this.page = new UserLoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception{
		
	}
	
	@Test
	public void testLoginHeader() {
		assertEquals(page.getHeader(), "PLEASE LOG IN");
	}
	
	@Test
	public void testSuccessfulRequest() throws InterruptedException {
		page.setUsername("aalouani");
		page.setPassword("password");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		driver.findElement(By.name("myrequests")).click();
		wait.until(ExpectedConditions.urlMatches("/myreimb_display.html"));
		List<WebElement> rows = driver.findElements(By.tagName("tr"));
		int sizeBefore = rows.size();
		System.out.println("height before: " + sizeBefore);
		driver.findElement(By.tagName("button")).submit();
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		driver.findElement(By.name("request")).click();
		wait.until(ExpectedConditions.urlMatches("/reimb_request.html"));
		driver.findElement(By.name("amount")).clear();
		driver.findElement(By.name("amount")).sendKeys("400");
		driver.findElement(By.name("description")).clear();
		driver.findElement(By.name("description")).sendKeys("test description");
		driver.findElement(By.name("reimbsubmit")).click();
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		driver.findElement(By.name("myrequests")).click();
		wait.until(ExpectedConditions.urlMatches("/myreimb_display.html"));
		rows = driver.findElements(By.tagName("tr"));
		int sizeAfter = rows.size();
		
		System.out.println("height After: " + sizeAfter);
		assertEquals(1, sizeAfter - sizeBefore);
	}
	
	@Test
	public void testSuccessfulLogin() {
		page.setUsername("aalouani");
		page.setPassword("password");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		
		assertEquals("http://localhost:9009/html/home.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testSuccessfulLoginManager() {
		page.setUsername("denis007");
		page.setPassword("plouky");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/home_manager.html"));
		assertEquals("http://localhost:9009/html/home_manager.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testFailedLogin() {
		page.setUsername("aalouani");
		page.setPassword("wrong");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.urlMatches("/unsuccessfullogin.html"));
		assertEquals("http://localhost:9009/html/unsuccessfullogin.html", driver.getCurrentUrl());
	}

}
