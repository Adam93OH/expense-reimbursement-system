package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.DBConnection;
import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;
import com.example.model.User;

public class ReimbDaoTest {
	

	@Mock
	private DBConnection dbc;
	
//	@Mock
//	private DriverManager dm;
	
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	private User testUser;
	private Reimbursement testReimb;
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dbc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		Timestamp t = null;
		Blob b = null;
		testReimb = new Reimbursement(1, 293.45, t, t, "description", b, 1, 2, 2, 4);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testReimb.getReimbId());
		when(rs.getDouble(2)).thenReturn(testReimb.getAmount());
		when(rs.getTimestamp(3)).thenReturn(testReimb.getSubmitted());
		when(rs.getTimestamp(4)).thenReturn(testReimb.getResolved());
		when(rs.getString(5)).thenReturn(testReimb.getDescription());
		when(rs.getBlob(6)).thenReturn(testReimb.getReceipt());
		when(rs.getInt(7)).thenReturn(testReimb.getAuthor());
		when(rs.getInt(8)).thenReturn(testReimb.getResolver());
		when(rs.getInt(9)).thenReturn(testReimb.getStatusId());
		when(rs.getInt(10)).thenReturn(testReimb.getTypeId());
		when(ps.executeQuery()).thenReturn(rs);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByNameSuccess() {
		assertEquals(new ReimbursementDao(dbc).findById(1).getDescription(), testReimb.getDescription());
	}


}
