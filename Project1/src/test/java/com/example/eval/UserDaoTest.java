package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.DBConnection;
import com.example.dao.UserDao;
import com.example.model.User;

public class UserDaoTest {
	
	
	@Mock
	private DBConnection dbc;
	
//	@Mock
//	private DriverManager dm;
	
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dbc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testUser = new User(1,"aalouani","password", "Adam", "Alouani", "aalouani@gmail.com", 1);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getUsersId());
		when(rs.getString(2)).thenReturn(testUser.getUsername());
		when(rs.getString(3)).thenReturn(testUser.getPassword());
		when(rs.getString(4)).thenReturn(testUser.getFirstName());
		when(rs.getString(5)).thenReturn(testUser.getLastName());
		when(rs.getString(6)).thenReturn(testUser.getEmail());
		when(rs.getInt(7)).thenReturn(testUser.getUserRoleId());
		when(ps.executeQuery()).thenReturn(rs);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByNameSuccess() {
		assertEquals(new UserDao(dbc).findById(1).getUsername(), testUser.getUsername());
	}


}
