package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.User;

public class UserDao implements GenericDao<User> {

	private DBConnection dbc;

	public UserDao() {
		// TODO Auto-generated constructor stub
	}

	public UserDao(DBConnection dbc) {
		super();
		this.dbc = dbc;
	}

	@Override
	public User findById(int id) {
		User u = null;

		try (Connection con = dbc.getDBConnection()) {

			String sql = "SELECT * FROM ERS_USERS WHERE ERS_USERS_ID =?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return u;
			}

			u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return u;
	}
	
	@Override
	public List<User> selectAll() {
		List<User> uList = new ArrayList<>();
		
		try (Connection con = dbc.getDBConnection()) {

			String sql = "SELECT * FROM ERS_USERS";

			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				uList.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return uList;
		
	}
	
	public User findByName(String username) {
		User u = null;

		try (Connection con = dbc.getDBConnection()) {

			String sql = "SELECT * FROM ERS_USERS WHERE ERS_USERNAME =?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();

			if (!rs.first()) {
				return u;
			}

			u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return u;
	}

	@Override
	public void insert(User entity) {
		try (Connection con = dbc.getDBConnection()) {

			String sql = "{ CALL insert_user(?,?,?,?,?,?) }";

			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, entity.getUsername());
			cs.setString(2, entity.getPassword());
			cs.setString(3, entity.getFirstName());
			cs.setString(4, entity.getLastName());
			cs.setString(5, entity.getEmail());
			cs.setInt(6, entity.getUserRoleId());

			int status = cs.executeUpdate();
			System.out.println("Callable statement returns: " + status);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}



}
