package com.example.dao;

import java.util.List;

public interface GenericDao <E> {

	public E findById(int id);
	public void insert(E entity);
	public List<E> selectAll();
	
}