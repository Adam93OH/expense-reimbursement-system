package com.example.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimbursement;

public class ReimbursementDao implements GenericDao<Reimbursement> {

	private DBConnection dbc = new DBConnection();
	private UserDao uDao = new UserDao();
	
	public ReimbursementDao() {
		// TODO Auto-generated constructor stub
	}
	

	public ReimbursementDao(DBConnection dbc) {
		super();
		this.dbc = dbc;
	}



	@Override
	public Reimbursement findById(int id) {
		Reimbursement r = null;
		try (Connection con = dbc.getDBConnection()) {

			String sql = "SELECT * FROM ERS_REIMBURSEMENT WHERE REIMB_ID = ?";

			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setInt(1, id);

			ResultSet rs = prepStatement.executeQuery();
			if (rs.first())
				r = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10));
			else
				r = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return r;
	}

	@Override
	public void insert(Reimbursement entity) {
		try (Connection con = dbc.getDBConnection()) {

			String sql = "{ CALL insert_reimbursement(?,?,?,?,?,?,?,?,?) }";

			CallableStatement cs = con.prepareCall(sql);
			cs.setDouble(1, entity.getAmount());
			cs.setTimestamp(2, entity.getSubmitted());
			cs.setTimestamp(3, entity.getResolved());
			cs.setString(4, entity.getDescription());
			cs.setBlob(5, entity.getReceipt());
			cs.setInt(6, entity.getAuthor());
			cs.setInt(7, entity.getResolver());
			cs.setInt(8, entity.getStatusId());
			cs.setInt(9, entity.getTypeId());

			int status = cs.executeUpdate();
			System.out.println("Callable statement returns: " + status);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public List<Reimbursement> selectAllPending() {
		List<Reimbursement> rList = new ArrayList<>();
		
		try (Connection con = dbc.getDBConnection()) {

			String sql = "SELECT * FROM ERS_REIMBURSEMENT WHERE REIMB_STATUS_ID = 1";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rList;
		
	}
	@Override
	public List<Reimbursement> selectAll() {
		List<Reimbursement> rList = new ArrayList<>();
		
		try (Connection con = dbc.getDBConnection()) {

			String sql = "SELECT * FROM ERS_REIMBURSEMENT";

			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rList;
		
	}
	
	public void updateReimbStatus(int id, int status, int res) {
		
		try (Connection con = dbc.getDBConnection()) {

			String sql = "UPDATE ERS_REIMBURSEMENT SET REIMB_STATUS_ID = ?, REIMB_RESOLVER= ?, REIMB_RESOLVED = ? WHERE REIMB_ID = ?";

			long now = System.currentTimeMillis();
	        Timestamp time = new Timestamp(now);
	        
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, status);
			ps.setInt(2, res);
			ps.setTimestamp(3, time);
			ps.setInt(4, id);
			int s = ps.executeUpdate();
			System.out.println("Callable statement returns: " + s);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public List<Reimbursement> selectAllbyAuthor(int id) {
		List<Reimbursement> rList = new ArrayList<>();
		
		try (Connection con = dbc.getDBConnection()) {

			String sql = "SELECT * FROM ERS_REIMBURSEMENT WHERE REIMB_AUTHOR = ?";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rList;
		
	}

}
