package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	private String url = "jdbc:mariadb://database-1.c9wklnisu4gn.us-east-2.rds.amazonaws.com:3306/project1db";
	private String username = "proj1user";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}


}
