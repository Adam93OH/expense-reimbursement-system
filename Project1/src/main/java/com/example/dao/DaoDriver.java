package com.example.dao;


import java.sql.Blob;
import java.sql.Timestamp;

import com.example.model.Reimbursement;
import com.example.model.User;

public class DaoDriver {
	
	public static void main(String[] args) {
		
		DBConnection dbc = new DBConnection();
		UserDao ud = new UserDao(dbc);
		
		ReimbursementDao rd = new ReimbursementDao(dbc);
		
		//System.out.println(ud.findById(1));
		
//		User u6 = new User("atp", "tennisboss", "A", "TP", "atp@gmail.com", 2);
//		User u = new User("aalouani", "password", "Adam", "Alouani", "aalouani@gmail.com", 1);
//		User u2 = new User("denis007", "plouky", "Denis", "Alouani", "denis007@gmail.com", 1);
//		User u3 = new User("renÚlacoste", "crocodile", "RenÚ", "Lacoste", "renÚlacoste@gmail.com", 1);
//		User u4 = new User("rafanadal", "rolandgarros", "Rafael", "Nadal", "rafa@gmail.com", 1);
//		User u5 = new User("rogerfederer", "tennis", "Roger", "Federer", "rf@gmail.com", 1);
		User u7 = new User("bossman", "boss123", "Boss", "Man", "bs@gmail.com", 2);
//		long now = System.currentTimeMillis();
//        Timestamp sqlTimestamp = new Timestamp(now);
//        Blob b = null;
//        ud.insert(u);
//        ud.insert(u2);
//        ud.insert(u3);
//        ud.insert(u4);
//        ud.insert(u5);
        ud.insert(u7);
//		Reimbursement r = new Reimbursement(475.70, sqlTimestamp, sqlTimestamp, "description", b, 1, 2, 2, 1);
//        rd.insert(r);
//        System.out.println(rd.selectAll());
//		System.out.println(ud.findByName("aalouani"));
		
	}

}
