package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.User;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class ReimbursementController {
	
	private ReimbursementService rServ;
	private UserService uServ;
	
	public ReimbursementController() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}
	
	public Handler REIMBGETALLPENDING = (ctx)->{
		if(ctx.sessionAttribute("user") == null)
			ctx.status(404);
		else {
			List<Reimbursement> rList = new ArrayList<>();
//			List<User> uList = new ArrayList<>();
//			uList = uServ.getAllUsers();
			rList = rServ.getAllPendingReimbursements();
			ctx.json(rList);
			ctx.status(200);
//			ctx.sessionAttribute("reimbursements", rList);			
		}
//		ctx.redirect("/html/reimb_display.html");
		
		
	};
	
	public Handler REIMBGETALL = (ctx)->{
		if(ctx.sessionAttribute("user") == null)
			ctx.status(404);
		else {
			List<Reimbursement> rList = new ArrayList<>();
			rList = rServ.getAllReimbursements();
			ctx.json(rList);
			ctx.status(200);
			//ctx.sessionAttribute("reimbursements", rList);			
		}
//		ctx.redirect("/html/reimb_display.html");
		
		
	};
	
	public Handler REIMBGETALLBYID = (ctx)->{
		List<Reimbursement> rList = new ArrayList<>();
		int author = ((User)ctx.sessionAttribute("user")).getUsersId();
		rList = rServ.getReimbursementsById(author);
		ctx.json(rList);
		ctx.status(200);
		ctx.sessionAttribute("reimbursements", rList);
		
		
	};
	
	public Handler UPDATEREIMBSTATUSAPPROVE = (ctx)->{
		int status = 2;
		int id = Integer.parseInt(ctx.formParam("approve"));
		int res = ((User)ctx.sessionAttribute("user")).getUsersId();
		rServ.updateReimbStatus(id, status, res);
		ctx.redirect("/html/approves.html");
	};
	
	public Handler UPDATEREIMBSTATUSDENY = (ctx)->{
		int status = 3;
		int id = Integer.parseInt(ctx.formParam("deny"));
		int res = ((User)ctx.sessionAttribute("user")).getUsersId();
		rServ.updateReimbStatus(id, status, res);
		ctx.redirect("/html/denies.html");
	};
	
	public Handler INSERTREIMB = (ctx)->{
		double amount = Double.parseDouble(ctx.formParam("amount"));
		String description = ctx.formParam("description");
		String type = ctx.formParam("rtype");
		
		int author = ((User)ctx.sessionAttribute("user")).getUsersId();
		System.out.println(author);
		rServ.addReimbursement(amount, description, type.toUpperCase(), author);
		ctx.redirect("/html/home.html");
		
	};

}
