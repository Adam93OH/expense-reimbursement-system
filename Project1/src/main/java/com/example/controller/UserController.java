package com.example.controller;

import com.example.model.User;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public Handler postLogin = (ctx) ->{
//		System.out.println(ctx.body());
		if(uServ.userVerify(ctx.formParam("username"), ctx.formParam("password"))) {
			if(uServ.userVerifyManager(ctx.formParam("username"))) {
				System.out.println("User is a verified Finance Manager");
				ctx.sessionAttribute("user", uServ.getUserByName(ctx.formParam("username")));
				ctx.redirect("/html/home_manager.html");
			}
			else {
				System.out.println("User is verified");
				ctx.sessionAttribute("user", uServ.getUserByName(ctx.formParam("username")));
				ctx.redirect("/html/home.html");
			}
		}else {
			System.out.println("User is not verified");
			ctx.redirect("/html/unsuccessfullogin.html");
		}
	};
	
	public Handler LOGOUT = (ctx)->{
		ctx.sessionAttribute("user", null);
		ctx.redirect("/html/index.html");
	};
	
	public Handler getSessUser = (ctx)->{
		System.out.println((User)ctx.sessionAttribute("user"));
		User u = (User)ctx.sessionAttribute("user");
		ctx.json(u);
	};

}
