package com.example.service;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.List;

import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;

public class ReimbursementService {

	private ReimbursementDao rDao = new ReimbursementDao();

	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}

	public List<Reimbursement> getAllPendingReimbursements() {
		List<Reimbursement> rList = rDao.selectAllPending();
		if (rList == null) {
			throw new NullPointerException();
		}
		return rList;
	}
	
	public List<Reimbursement> getAllReimbursements() {
		List<Reimbursement> rList = rDao.selectAll();
		if (rList == null) {
			throw new NullPointerException();
		}
		return rList;
	}
	
	public List<Reimbursement> getReimbursementsById(int id) {
		List<Reimbursement> rList = rDao.selectAllbyAuthor(id);
		if (rList == null) {
			throw new NullPointerException();
		}
		return rList;
	}
	
	public void updateReimbStatus(int id, int status, int res) {
		rDao.updateReimbStatus(id, status, res);
	}

	public void addReimbursement(double amount, String description, String type, int author) {
		int t = 1;
		if(type.equals("TRAVEL"))
			t=2;
		if(type.equals("FOOD"))
			t=3;
		if(type.equals("OTHER"))
			t=4;
		
		long now = System.currentTimeMillis();
		Timestamp submitted = new Timestamp(now);
		Timestamp resolved = null;
		Blob receipt = null;
		Reimbursement r = new Reimbursement(amount, submitted, resolved, description, receipt, author, 1, 1, t);
		rDao.insert(r);
	}

}
