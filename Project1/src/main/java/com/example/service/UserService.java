package com.example.service;

import java.util.List;

import com.example.dao.UserDao;
import com.example.model.User;

public class UserService {
	
	private UserDao uDao = new UserDao();
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}

	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}
	
	public User getUserById(int id) {
		User u = uDao.findById(id);
		if(u==null) {
			throw new NullPointerException();
		}
		return u;
	}
	
	public List<User> getAllUsers() {
		List<User> uList = uDao.selectAll();
		if (uList == null) {
			throw new NullPointerException();
		}
		return uList;
	}
	
	public User getUserByName(String username) {
		User u = uDao.findByName(username);
		if(u==null) {
			throw new NullPointerException();
		}
		return u;
	}
	
	public boolean userVerify(String username, String password) {
		 boolean isVerified = false;
		 User u = this.getUserByName(username);
		 if(u.getPassword().equals(password)) {
			 isVerified=true;
		 }
		 return isVerified;
	}
	
	public boolean userVerifyManager(String username) {
		 boolean isVerified = false;
		 User u = this.getUserByName(username);
		 if(u.getUserRoleId() == 2) {
			 isVerified=true;
		 }
		 return isVerified;
	}

}
