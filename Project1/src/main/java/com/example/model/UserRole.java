package com.example.model;

public class UserRole {
	
	private int userRoleId;
	private String userRole;
	
	public UserRole() {
		// TODO Auto-generated constructor stub
	}



	public String getUserRole() {
		return userRole;
	}


	public int getUserRoleId() {
		return userRoleId;
	}

	@Override
	public String toString() {
		return "UserRole [userRoleId=" + userRoleId + ", userRole=" + userRole + "]";
	}
	
	

}
