package com.example.model;

public class ReimbursementType {
	
	private int reimbTypeId;
	private String reimbType;
	
	public ReimbursementType() {
		// TODO Auto-generated constructor stub
	}




	public String getReimbType() {
		return reimbType;
	}


	public int getReimbTypeId() {
		return reimbTypeId;
	}

	@Override
	public String toString() {
		return "ReimbursementType [reimbTypeId=" + reimbTypeId + ", reimbType=" + reimbType + "]";
	}
	
	

}
