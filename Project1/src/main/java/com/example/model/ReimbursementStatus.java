package com.example.model;

public class ReimbursementStatus {
	
	private int reimbStatusId;
	private String reimbStatus;
	
	public ReimbursementStatus() {
		// TODO Auto-generated constructor stub
	}


	public String getReimbStatus() {
		return reimbStatus;
	}


	public int getReimbStatusId() {
		return reimbStatusId;
	}

	@Override
	public String toString() {
		return "ReimbursementStatus [reimbStatusId=" + reimbStatusId + ", reimbStatus=" + reimbStatus + "]";
	}
	
	

}
