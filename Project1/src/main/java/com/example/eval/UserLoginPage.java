package com.example.eval;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UserLoginPage {
	
	private WebDriver driver;
	private WebElement header;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement submitButton;
	
	public UserLoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.header = driver.findElement(By.tagName("h1"));
		this.usernameField = driver.findElement(By.name("username"));
		this.passwordField = driver.findElement(By.name("password"));
		this.submitButton = driver.findElement(By.name("usersubmit"));
	}
	public void setUsername(String name) {
		this.usernameField.clear();
		this.usernameField.sendKeys(name);
	}
	
	public String getUsername() {
		return this.usernameField.getAttribute("value");
	}
	
	public void setPassword(String power) {
		this.passwordField.clear();
		this.passwordField.sendKeys(power);
	}
	
	public String getPassword() {
		return this.passwordField.getAttribute("value");
	}
	
	public String getHeader() {
		return this.header.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9009/html/index.html");
	}

}
