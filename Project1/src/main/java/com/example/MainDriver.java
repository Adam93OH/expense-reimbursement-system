package com.example;

import com.example.controller.ReimbursementController;
import com.example.controller.UserController;
import com.example.dao.DBConnection;
import com.example.dao.ReimbursementDao;
import com.example.dao.UserDao;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

import io.javalin.Javalin;

public class MainDriver {
	
public static void main(String[] args) {
		
		UserController uCon = new UserController(new UserService(new UserDao(new DBConnection())));
		ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new DBConnection())));
		Javalin app = Javalin.create(config ->{
			config.addStaticFiles("/frontend");
		});
		
		app.start(9009);
		
		app.post("/users/login", uCon.postLogin);
		app.get("/users/session", uCon.getSessUser);
		app.get("/reimbursements/all", rCon.REIMBGETALL);
		app.get("/reimbursements/pending", rCon.REIMBGETALLPENDING);
		app.post("/reimbursements", rCon.INSERTREIMB);
		app.get("/reimbursements/id", rCon.REIMBGETALLBYID);
		app.post("/reimbursements/approves", rCon.UPDATEREIMBSTATUSAPPROVE);
		app.post("/reimbursements/denies", rCon.UPDATEREIMBSTATUSDENY);
		app.get("/logout", uCon.LOGOUT);
		app.exception(NullPointerException.class, (e, ctx)->{
			ctx.status(404);
			ctx.redirect("/html/unsuccessfullogin.html");
		});

	}

}
