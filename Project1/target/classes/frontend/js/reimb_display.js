//console.log("js is linked");

window.onload = function() {
	getReimbSession();
}

function getFormattedDate(dateIn) {
	if (dateIn == null) {
		return "";
	};
	let date = new Date(dateIn);
	let month = date.getMonth() + 1;
	let day = date.getDate();
	let hour = date.getHours();
	let min = date.getMinutes();
	let sec = date.getSeconds();
	month = (month < 10 ? "0" : "") + month;
	day = (day < 10 ? "0" : "") + day;
	hour = (hour < 10 ? "0" : "") + hour;
	min = (min < 10 ? "0" : "") + min;
	sec = (sec < 10 ? "0" : "") + sec;
	let str = date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
	return str;
};

function generateTableHead(table, data) {
	let thead = table.createTHead();
	let row = thead.insertRow();
	for (let key of data) {
//		console.log(key);
		let th = document.createElement("th");
		let text = document.createTextNode(key);
		th.appendChild(text);
		row.appendChild(th);
	}
	let th = document.createElement("th");
	let text = document.createTextNode("Resolve");
	th.appendChild(text);
	row.appendChild(th);

}

function generateTable(table, data) {
	for (let element of data) {
		let row = table.insertRow();
		for (key in element) {
//			console.log(key);
			let e = element[key];
			if (key == "submitted" || key == "resolved")
				e = getFormattedDate(e);
			if (key == "statusId") {
				if (e == 1)
					e = "Pending";
				else if (e == 2)
					e = "Approved";
				else
					e = "Denied";
			}
			if (key == "resolver")
				if(e == 1)
					e = "Unresolved";
			if (key == "typeId") {
				if (e == 1)
					e = "Lodging";
				else if (e == 2)
					e = "Travel";
				else if (e == 3)
					e = "Food";
				else
					e = "Other";
			}
			let cell = row.insertCell();
			let text = document.createTextNode(e);
			cell.appendChild(text);
		}
		let cell = document.createElement('td');
		cell.innerHTML =
			'<form id="approve" method="post" action="/reimbursements/approves"> <input type="hidden" name="approve" value="'+ element["reimbId"]+'"> <button type="submit" class="btn btn-success" value="approve" name="usersubmit">Approve</button> </form>'
			+ '<form id="deny" method="post" action="/reimbursements/denies"> <input type="hidden" name="deny" value="'+ element["reimbId"]+'"> <button type="submit" class="btn btn-danger" value="deny" name="usersubmit">Deny</button> </form>';
		row.appendChild(cell);

	}
}

function getReimbSession() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
//			console.log(xhttp.responseText);
			var reimbursements = JSON.parse(xhttp.responseText);
//			console.log(reimbursements);

			let table = document.querySelector("table");
			let data = Object.keys(reimbursements[0]);
			generateTable(table, reimbursements);
			generateTableHead(table, data);


		}
	}

	xhttp.open("GET", "http://localhost:9009/reimbursements/pending");

	xhttp.send();

}